# slstc

relatively simple ficitonal solstice calculator

# use

Make sure the Haskell package manager, Cabal, is installed on your system:
`cabal --version`

Clone in the repo:
`git clone https://gitlab.com/oddx/slstc.git`

Build the utility:
`cabal build`

Run it sonnn:
`cabal run slstc a b c d e f [x y]`
* a = arbitrary hours per day
* b = true hours per day
* c = true days per solstice
* d = start year
* e = end year
* f = seed
* x = true days per lunar cycle
* y = beginning of first lunar cycle
