import System.Environment
import System.Random

main = getArgs >>= parse

justSolstice args = do
   let [arbDay, trueDay, trueSolstice] = readList' (take 3 args)           :: [Double]
   let [start, end, seed]              = readList' (take 3 (drop 3 args))  :: [Int]
   let randomness    = take (end + 1) (drop start (randoms $ mkStdGen seed :: [Double]))
   let offset        = calcOffset arbDay trueDay
   let currySolstice = calcArbSolstice trueSolstice offset arbDay
   let cal           = drop 1 (map ceiling (zipWith (+) randomness (generateCal currySolstice trueSolstice start (end + 1))))

   etch cal [] start end

lunarAndSolstice args = do
   let [arbDay, trueDay, trueSolstice] = readList' (take 3 args)           :: [Double]
   let [start, end, seed]              = readList' (take 3 (drop 3 args))  :: [Int]
   let [lunarCycle, lunarStart]        = readList' (drop 6 args)           :: [Double]
   let randomness    = take (end + 1) (drop start (randoms $ mkStdGen seed :: [Double]))
   let offset        = calcOffset arbDay trueDay
   let currySolstice = calcArbSolstice trueSolstice offset arbDay
   let cal           = drop 1 (map ceiling (zipWith (+) randomness (generateCal currySolstice trueSolstice start (end + 1))))
   let curryLunar    = calcLunarCycle lunarCycle trueSolstice
   let lunarFactor   = ceiling $ trueSolstice / lunarCycle
   let lVariance     = take (end * lunarFactor) (drop (start * lunarFactor) (randoms $ mkStdGen (seed + 1) :: [Double]))
   let lunarCal      = generateCal curryLunar lunarStart (start * lunarFactor) (end * lunarFactor)
   let rLunarCal     = map ceiling (zipWith (+) lVariance lunarCal)
   let fLunarCal     = splitByIndices rLunarCal (localMinima rLunarCal 0) 

   etch cal fLunarCal start end

type Anniversary     = Double
type ArbDay          = Double
type Iteration       = Double
type Offset          = Double
type RawArbSolstice  = Double
type TrueDay         = Double
type TrueLunarCycle  = Double
type TrueSolstice    = Double

readList' :: Read a => [String] -> [a]
readList' xs = map read xs

calcOffset :: ArbDay -> TrueDay -> Offset
calcOffset x y = x - y

localMinima :: (Num a, Ord a) => [a] -> a -> [a]
localMinima (x:rest@(y:z:_)) i
   | y < x && y < z    = (i + 1): localMinima rest (i + 1)
   | otherwise = localMinima rest (i + 1)
localMinima _ i = []

splitByIndices :: (Num a, Eq a) => [a] -> [Int] -> [[a]]
splitByIndices xs is = go xs (zipWith subtract (0:is) is) where
   go  [] _      = []
   go  xs (i:is) = let (a, b) = splitAt i xs in a : go b is
   go  xs _      = [xs]

--add time (hours) accrued over solstice period to solstice period, plus or minus last year's calendar difference 
calcArbSolstice :: TrueSolstice -> Offset -> ArbDay -> RawArbSolstice -> RawArbSolstice
calcArbSolstice s o a r = ((s * o) / a + s) + (realToFrac . ceiling $ r) - r

calcLunarCycle :: TrueLunarCycle -> TrueSolstice -> Anniversary -> Anniversary
calcLunarCycle l s x
             | ceiling (l + x) > ceiling s  = (l + x) - s
             | ceiling (l + x) < ceiling s  = l + x 
             | ceiling (l + x) == ceiling s = (2 * l + x) - s

generateCal :: (a -> a) -> a -> Int -> Int -> [a]
generateCal f x start end = take end (drop start (iterate f x))

parse :: [String] -> IO [()]
parse xs
      | length xs == 6 = justSolstice xs
      | length xs == 8 = lunarAndSolstice xs
      | otherwise = sequence [putStrLn "Please run 'slstc a b c d e f [x y]' -- hours per arb day, hours per true day, true days per solstice, start year, end year, random seed, and optionally, true days per lunar cycle with lunar anniversary arb start day"]


etch :: [Int] -> [[Int]] -> Int -> Int -> IO [()]
etch cal [] start end   = sequence (zipWith  (\x y   -> putStrLn ("year " ++ show y ++ " | " ++ show x ++ " days")) cal [start..end])
etch cal lcal start end = sequence (zipWith3 (\x y z -> putStrLn ("year " ++ show z ++ " | " ++ show x ++ " days | moon cycles on " ++ show y)) cal lcal [start..end])
